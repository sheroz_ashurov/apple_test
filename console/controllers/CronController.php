<?php

namespace console\controllers;

use backend\models\Apple;
use yii\console\Controller;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CronController extends Controller
{
    public function actionCheckApple(){
        $apples = Apple::find()->where('status=:status', [':status' => Apple::STATUS_ON_THE_GROUND])->all();
        foreach ($apples as $apple){
            $differenceTime = time() - $apple->fell_at;
            $percent = (float)((int)($differenceTime / 180)) / 100;
            $fresh = $apple->size - $apple->spoiled;
            $apple->spoiled += $percent;
            if($differenceTime >= 18000 || $fresh <= 0 || $apple->spoiled >= 1){
                $apple->status = Apple::STATUS_ROTTEN;
            }
            $apple->save();
        }
    }
}
