<?php

use yii\db\Migration;

/**
 * Class m210315_193358_add_admin
 */
class m210315_193358_add_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user', [
            'id' => 1,
            'username' => 'admin',
            'auth_key' => 'gOlP3qMOqc7tnC9yIkqdCsTh74RoJdB6',
            'password_hash' => '$2y$13$I8qqhga1NJ3D/wk4H0df/eDiokBi73LwdFd7yYW.Ld6RgIRotiARm',
            'email' => 'admin@admin.ru',
            'created_at' => time(),
            'updated_at' => time(),
            'status' => 10,
            'verification_token' => 'wCrGbq8_U-QGvDrDk1TGdXcLC_o7QC-L_1615820431',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210315_193358_add_admin cannot be reverted.\n";

        return false;
    }
    */
}
